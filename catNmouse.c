///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Nathaniel Murray <murrayn@hawaii.edu>
/// @date    1/26/2022
///////////////////////////////////////////////////////////////////////////////


#define DEFAULT_MAX_NUMBER 2048 
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main( int argc, char* argv[] ) {
   printf( "Cat `n Mouse\n" );
   printf( "The number of arguments is: %d\n", argc );

   // This is an example of getting a number from a user
   // Note:  Don't enter unexpected values like 'blob' or 1.5.
   
   int theMaxValue = DEFAULT_MAX_NUMBER;
   int aGuess;
   int MinValue = 1;
   int incorrectGuess = 1;

   srand(time(0));

   // seeded rand() function to time to add variation into array spaces being accessed
   int theNumberImThinkingOf = (rand() % (theMaxValue - MinValue + 1)) + MinValue;


   //do loop while guess is incorrect. When guess is not incorrect, loop exits
   do{
      printf( "OK cat, I'm thinking of a number from 1 to %d. Make a guess:\n \n", theMaxValue);
      scanf( "%d", &aGuess );

         if(aGuess < 1){
             printf("\nYou must enter a number that’s >= 1\n \n");
         continue;
         }

         if(aGuess > theMaxValue){
            printf("\nYou must enter a number that's <= 2048\n \n"); 
         continue;
         }

         if(aGuess > theNumberImThinkingOf){
            printf("\nNo cat... the number I'm thinking of is smaller than %d.\n \n", aGuess);
         continue;
         }

         if(aGuess < theNumberImThinkingOf){
            printf("\nNo cat... the number I'm thinking of is larger than %d.\n \n", aGuess);
         continue;
         }

         if(aGuess = theNumberImThinkingOf){
            printf("\nYou got me.\n \n"); 

printf("          |\\___/| \n");
printf("         =) ^ ^ (= \n");                       
printf("          \\  ^  / \n");
printf("           )=*=( \n");      
printf("          /     \\ \n");
printf("          |     |\\ \n");
printf("         /| | | |\\ \n");
printf("         \\| | |_|/\\ \n");
printf("          |_|_|_|_/ \n");






            incorrectGuess = 0;
         }
      }
      while(incorrectGuess);
   

   return 0;
}

